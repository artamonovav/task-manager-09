package ru.t1.artamonov.tm.component;

import ru.t1.artamonov.tm.api.ICommandController;
import ru.t1.artamonov.tm.api.ICommandRepository;
import ru.t1.artamonov.tm.api.ICommandService;
import ru.t1.artamonov.tm.controller.CommandController;
import ru.t1.artamonov.tm.repository.CommandRepository;
import ru.t1.artamonov.tm.service.CommandService;

import java.util.Scanner;

import static ru.t1.artamonov.tm.constant.ArgumentConst.*;
import static ru.t1.artamonov.tm.constant.ArgumentConst.ARG_COMMANDS;
import static ru.t1.artamonov.tm.constant.TerminalConst.*;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private void processCommand(final String command) {
        if (command == null) {
            commandController.showErrorCommand();
            return;
        }
        switch (command) {
            case CMD_HELP:
                commandController.showHelp();
                break;
            case CMD_VERSION:
                commandController.showVersion();
                break;
            case CMD_ABOUT:
                commandController.showAbout();
                break;
            case CMD_INFO:
                commandController.showInfo();
                break;
            case CMD_EXIT:
                System.exit(0);
                break;
            case CMD_ARGUMENTS:
                commandController.showArguments();
                break;
            case CMD_COMMANDS:
                commandController.showCommands();
                break;
            default:
                commandController.showErrorCommand();
                break;
        }
    }

    private void processCommands() {
        commandController.showWelcome();
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.print("\nENTER COMMAND: ");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    private void processArgument(final String argument) {
        if (argument == null) {
            commandController.showErrorArgument();
            return;
        }
        switch (argument) {
            case ARG_HELP:
                commandController.showHelp();
                break;
            case ARG_VERSION:
                commandController.showVersion();
                break;
            case ARG_ABOUT:
                commandController.showAbout();
                break;
            case ARG_INFO:
                commandController.showInfo();
                break;
            case ARG_ARGUMENTS:
                commandController.showArguments();
                break;
            case ARG_COMMANDS:
                commandController.showCommands();
                break;
            default:
                commandController.showErrorArgument();
                break;
        }
    }

    private boolean processArguments(final String[] args) {
        if (args == null || args.length < 1) {
            return false;
        }
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

    public void run (final String[] args) {
        if (processArguments(args)) System.exit(0);
        processCommands();
    }

}
