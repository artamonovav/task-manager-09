package ru.t1.artamonov.tm;

import ru.t1.artamonov.tm.component.Bootstrap;

public final class Application {

    public static void main(String[] args) {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
