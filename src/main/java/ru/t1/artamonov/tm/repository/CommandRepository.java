package ru.t1.artamonov.tm.repository;

import ru.t1.artamonov.tm.api.ICommandRepository;
import ru.t1.artamonov.tm.constant.ArgumentConst;
import ru.t1.artamonov.tm.constant.TerminalConst;
import ru.t1.artamonov.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    public static Command ABOUT = new Command(TerminalConst.CMD_ABOUT, ArgumentConst.ARG_ABOUT,
            "Display developer info."
    );

    public static Command EXIT = new Command(TerminalConst.CMD_EXIT, null,
            "Close application."
    );

    public static Command HELP = new Command(TerminalConst.CMD_HELP, ArgumentConst.ARG_HELP,
            "Display list of terminal commands."
    );

    public static Command INFO = new Command(TerminalConst.CMD_INFO, ArgumentConst.ARG_INFO,
            "Display info."
    );

    public static Command VERSION = new Command(TerminalConst.CMD_VERSION, ArgumentConst.ARG_VERSION,
            "Display program version."
    );

    public static Command COMMANDS = new Command(TerminalConst.CMD_COMMANDS, ArgumentConst.ARG_COMMANDS,
            "Display program commands."
    );

    public static Command ARGUMENTS = new Command(TerminalConst.CMD_ARGUMENTS, ArgumentConst.ARG_ARGUMENTS,
            "Display program arguments."
    );

    private static final Command[] TERMINAL_COMMANDS = new Command[] {
            INFO, ABOUT, ARGUMENTS, COMMANDS, VERSION, HELP, EXIT
    };

    @Override
    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
